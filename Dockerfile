FROM ubuntu:latest

MAINTAINER Bharani Dharan

RUN apt-get update -y \
 && apt-get install wget -y \
 && apt-get install unzip -y \
 && apt-get install -y python3-pip \
 && pip3 install --upgrade pip \
 && python3 -V \
 && pip --version \
 && pip install awscli \
 && pip install psycopg2-binary \
 && pip install boto3
ADD /src/app.py /home/app.py
CMD ["/bin/bash"]

