# PythonApp
  * Connection properties
  * RDS version

# Docker image version
  registry.gitlab.com/bharanisrec7/pythonapp:latest

# Contains
  * python3
  * pip
  * psycopg2-binary
  * boto3
  * awscli
