import psycopg2
import boto3

ssm = boto3.client('ssm', region_name='us-east-1')

dbusername = ssm.get_parameter(Name='/production/database/username/master', WithDecryption=True)
dbpassword = ssm.get_parameter(Name='/production/database/password/master', WithDecryption=True)
dbname = ssm.get_parameter(Name='/production/database/dbname/master', WithDecryption=True)
dbhost = ssm.get_parameter(Name='/production/database/dbhost/master', WithDecryption=True)

conn = psycopg2.connect(
    database=dbname['Parameter']['Value'],
    user=dbusername['Parameter']['Value'],
    password=dbpassword['Parameter']['Value'],
    host=dbhost['Parameter']['Value'].split(":")[0],
    port=dbhost['Parameter']['Value'].split(":")[1]
)

conn.autocommit = True

cursor = conn.cursor()
cursor.execute('SELECT VERSION()')
row = cursor.fetchone()
print(conn.get_dsn_parameters(),"\n")
print(row)
cursor.close()

#Closing the connection
conn.close()
